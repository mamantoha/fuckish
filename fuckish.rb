# frozen_string_literal: true

require 'twitter'
require 'matiuky_regexp'
require 'json'
require 'pry'

require 'dotenv'
Dotenv.load('.envrc')

user_name = ENV['TWITTER_USERNAME']
current_year = Time.now.year
since_time = Time.new(current_year)

# https://apps.twitter.com/
client = Twitter::REST::Client.new do |config|
  config.consumer_key        = ENV['CONSUMER_KEY']
  config.consumer_secret     = ENV['CONSUMER_SECRET']
  config.access_token        = ENV['ACCESS_TOKEN']
  config.access_token_secret = ENV['ACCESS_SECRET']
end

# You can fetch up to 3,200 tweets for a user, 200 at a time.
if user_name
  user = client.user(user_name)
  statuses_count = user.statuses_count
  current_user_id = user.id
else
  statuses_count = client.user.statuses_count
  current_user_id = client.user.id
end

puts "User: #{user_name}"
puts "Total tweets: #{statuses_count}"

collection = []
max_id = nil

options = { count: 200, include_rts: true }

loop do
  options[:max_id] = max_id unless max_id.nil?
  response = client.user_timeline(current_user_id, options)
  collection += response

  break if response.empty?
  break if response.last.created_at < since_time

  max_id = response.last.id - 1
end

collection.flatten!
collection.reject!(&:retweeted?)
collection.reject! { |tweet| tweet.created_at < since_time }

profanity_regexp = MatiukyRegexp::REGEXP

bad_words_collection = collection.select do |tweet|
  tweet.text =~ profanity_regexp
end

words = Hash.new { |h, k| h[k] = 0 }

bad_words_collection.each do |tweet|
  words_arry = tweet.text.scan(profanity_regexp)
  words_list = words_arry.map(&:first)

  words_list.each do |word|
    word = word.downcase
    word.gsub!(/[^[:alnum:]]/, '')
    words[word] += 1
  end
end

puts "Tweets in #{current_year}: #{collection.size}"
puts "Tweets with matiuky: #{bad_words_collection.size}"
puts "Unique matiuky this year: #{words.size}"
frequency = (bad_words_collection.size.to_f / collection.size) * 100
puts format('Frequency: %<frequency>.2f%%', frequency: frequency)

words_ordered = words.sort_by { |_, v| v }.reverse

js_word_array = words_ordered.reduce([]) do |arry, hsh|
  arry << { text: hsh[0], weight: hsh[1] }
end

puts js_word_array.to_json
